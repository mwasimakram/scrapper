var express = require('express');
var router = express.Router();
var axios = require('axios');
const xpath = require("xpath-html");


/* GET home page. */
router.get('/', function (req, res, next) {
  res.render("index", { title: "Scrapper" });
});

router.get("/getpage/:url", function (req, res, next) {
  let url = req.params.url
  var config = {
    method: 'get',
    url: `https://api.proxycrawl.com/?token=w-5v2ctumH6rWGvvVobb7Q&url=${url}`,
    headers: {}
  };
  axios(config)
    .then(function (response) {
      // const title = xpath.fromPageSource(response.data).findElement("(//*[@class='a-size-medium a-color-base a-text-normal'])");

      // console.log(title.childNodes)
      res.send(`${response.data}`);
    })
    .catch(function (error) {
      console.log(error);
    });
})

router.post("/saveData", (req, res, next) => {
  data_model.saveData(req.body, function (result) {
    res.send(JSON.stringify({ status: true }));
  });
})

module.exports = router;
